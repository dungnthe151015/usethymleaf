package com.example.usethymleaf.controller;

import com.example.usethymleaf.entity.Todo;
import com.example.usethymleaf.service.TodoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class TodoController {
    private final TodoService todoService;
//show
    @GetMapping("/todos")
    public String getAllToDo(Model model){
        List<Todo> todoList = todoService.getAll();
        model.addAttribute("todoList",todoList);
        return "todo";
    }
    //add
    @GetMapping("/add-todo")
    public String addToDo(Model model){

        model.addAttribute("todo",Todo.builder().build());
        return "addTodo";
    }

    @PostMapping("/add-todo")
    public String saveToDo(@ModelAttribute Todo todo){
        todoService.save(todo);
        return "redirect:/todos";
    }
    //delete
    @GetMapping("/delete-todo")
    public String deleteToDo(@RequestParam int id){
          todoService.deleteById(id);
        return "redirect:/todos";
    }

}
