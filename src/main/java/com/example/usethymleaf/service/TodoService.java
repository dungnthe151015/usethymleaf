package com.example.usethymleaf.service;

import com.example.usethymleaf.entity.Todo;
import com.example.usethymleaf.repository.TodoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TodoService {
    private final TodoRepository todoRepository;
    public List<Todo> getAll() {
        return todoRepository.getAll();
    }

    public void save(Todo todo) {
        todoRepository.save(todo);
    }

    public void deleteById(int id) {
        todoRepository.deleteById(id);
    }
}
