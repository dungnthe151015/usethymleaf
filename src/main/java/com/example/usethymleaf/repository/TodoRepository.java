package com.example.usethymleaf.repository;

import com.example.usethymleaf.entity.Todo;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class TodoRepository {
    //thay the tam DB
    private List<Todo> todoList;

    public TodoRepository (){
        todoList = new ArrayList<>();
                todoList.add(Todo.builder()
                .id(1)
                .title("monday")
                .detail("goto school")
                .build());
    }

    public List<Todo> getAll() {
        return todoList;
    }

    public void save(Todo todo) {
        int id=0;
        if(todoList.size() > 0){
           id = todoList.get(todoList.size()-1).getId()+1;

        }
        todo.setId(id);
        todoList.add(todo);
    }

    public void deleteById(int id) {
        for (int i = 0;i <todoList.size();i++){
            if(todoList.get(i).getId() == id)
           todoList.remove(i);
        }
    }
}
